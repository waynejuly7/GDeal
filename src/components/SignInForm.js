import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { FormLabel, FormInput, Button } from 'react-native-elements';
import axios from 'axios';
import firebase from 'firebase';

const ROOT_URL = 'https://us-central1-one-time-password-b9dab.cloudfunctions.net';
class SignInForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phone: '',
            code: ''
        }
    }

    handleSubmit = async () => {
        console.log(this.state.phone)
        console.log(this.state.code)
        try {
            let response = await axios.post(`${ROOT_URL}/verifyOneTimePassword`, JSON.stringify({
                phone: this.state.phone, code: this.state.code
            }))
            console.log(response)
            //firebase.auth().signInWithCustomToken(data.token) 
        } catch (err) {
            console.log(err)
        }
        
    }

    render() {
        return (
            <View>
                <View style={{marginBottom: 10}}>
                    <FormLabel>Enter Phone Number </FormLabel>
                    <FormInput 
                        value={this.state.phone}
                        onChangeText={phone => this.setState({phone})}/>
                </View>

                <View style={{marginBottom: 10}}>
                    <FormLabel>Enter Code </FormLabel>
                    <FormInput 
                        value={this.state.code}
                        onChangeText={code => this.setState({code})}/>
                </View>

                <Button title="Submit"
                    onPress={this.handleSubmit}/>
            </View>
        )
    }
}

export default SignInForm