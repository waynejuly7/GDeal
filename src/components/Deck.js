
import React, { Component } from 'react';
import { 
    Text, 
    PanResponder, 
    Animated, 
    View, 
    Dimensions,
    LayoutAnimation,
    UIManager 
} from 'react-native';
import { Card, Button } from 'react-native-elements';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SWIPE_THRESHOLD = 0.25 * SCREEN_WIDTH;
const SWIPE_OUT_DURATION = 0.25;

class Deck extends Component {
    static defaultProps = {
        onSwipeRight: () => {},
        onSwipeLeft: () => {}
    }

    constructor(props) {
        super(props);

        const position = new Animated.ValueXY();

        const panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => true, //execute each time user tap on the screen, 
            //it means each time user tap, panResponder will take responsible to handle
            onPanResponderMove: (event, gesture) => {
                position.setValue({ x: gesture.dx, y: gesture.dy });
            },
            onPanResponderRelease: (event, gesture) => {
                if (gesture.dx > SWIPE_THRESHOLD) {
                    console.log('swipe right')
                    this.forceSwipe('right');
                } else if (gesture.dx < -SWIPE_THRESHOLD) {
                    console.log('swipe left')
                    this.forceSwipe('left');
                } else {
                    this.resetPosition();
                }
            }
        });

        this.state = {
            panResponder,
            position,
            index: 0
        };
    }

    componentWillUpdate() {
        UIManager.setLayoutAnimationEnabledExperimental && 
        UIManager.setLayoutAnimationEnabledExperimental(true);

        LayoutAnimation.spring();
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.data !== this.props.data) {
            this.setState({ index: 0 })
        }
    }

    getCardStyle() {
        const { position } = this.state;
        //scale lại vị trí từ number sang degree theo trục X 
        const rotate = position.x.interpolate({
            inputRange: [-SCREEN_WIDTH * 1.5, 0, SCREEN_WIDTH * 1.5],
            outputRange: ['-120deg', '0deg', '120deg']
        })

        return {
            ...position.getLayout(),
            transform: [{rotate}]
        };
    }

    resetPosition() {
        Animated.spring(this.state.position, {
            toValue: {x: 0, y:0}
        }).start();
    }

    onSwipeComplete(direction) {
        const { onSwipeLeft, onSwipeRight, data } = this.props;
        const item = data[this.state.index];

        direction === 'right' ? onSwipeRight(item) : onSwipeLeft(item)
        this.state.position.setValue({ x:0, y:0 })
        this.setState({ index: this.state.index + 1});
    }

    forceSwipe(direction) {
        const x = direction === 'right' ? SCREEN_WIDTH : -SCREEN_WIDTH;

        Animated.timing(this.state.position, {
            toValue: {x, y: 0},
            duration: SWIPE_OUT_DURATION
        }).start(() => this.onSwipeComplete(direction));
    }

    renderCards() {
        if (this.state.index >= this.props.data.length) {
            return this.renderNoMoreCard();
        }
        return this.props.data.map((val, key) => {
            if (key < this.state.index) { return null;}
            if (key === this.state.index) {
                return  <Animated.View {...this.state.panResponder.panHandlers} style={[this.getCardStyle(), styles.cardStyle]} key={key}> 
                            <Card title={val.text} image={{ uri: val.uri }}>
                                <Text style={{ marginBottom: 10 }}>
                                    dqwdqwdw
                                </Text>
                                <Button icon={{ name: 'code' }} backgroundColor='#03A9F4' title="View !"/>
                            </Card>
                        </Animated.View>
            }
            return  <Animated.View key={key} style={[styles.cardStyle, {top: 10 * (key - this.state.index)}]}>
                        <Card title={val.text} image={{ uri: val.uri }} key={key}>
                            <Text style={{ marginBottom: 10 }}>
                                dqwdqwdw
                            </Text>
                            <Button icon={{ name: 'code' }} backgroundColor='#03A9F4' title="View !"/>
                        </Card>
                    </Animated.View> 
        }).reverse()
    }

    renderNoMoreCard() {
        return (
            <Card title="All done">
                <Text style={{ marginBottom: 10}}> No more content</Text>
                <Button backgroundColor="#03A9F4" title="Get more !"/>
            </Card>
        )
    }

    render() {
        return (
            this.renderCards()
        );
    }
}
const styles = {
    cardStyle: {
        position: 'absolute',
        width: SCREEN_WIDTH
    }
}

export default Deck;
