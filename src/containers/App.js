import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { navigationPropConstructor } from '../utils/redux';
import { initializeListeners } from 'react-navigation-redux-helpers';

import { AppNavigator } from '../navigators/AppNavigator'

// import firebase from 'firebase'

// import Deck from '../components/Deck';
// import SignUpForm from '../components/SignUpForm';
// import SignInForm from '../components/SignInForm';

// const { height } = Dimensions.get('window');
// const boxCount = 3;
// const boxHeight = height / boxCount;
// const data = [
//   { id: 1, text: 'Card #1', uri: 'https://i.ytimg.com/vi/cGFP4h1dD90/maxresdefault.jpg' },
//   { id: 2, text: 'Card #2', uri: 'https://i.ytimg.com/vi/cGFP4h1dD90/maxresdefault.jpg' },
//   { id: 3, text: 'Card #3', uri: 'https://i.ytimg.com/vi/cGFP4h1dD90/maxresdefault.jpg' },
//   { id: 4, text: 'Card #4', uri: 'https://i.ytimg.com/vi/cGFP4h1dD90/maxresdefault.jpg' },
//   { id: 5, text: 'Card #5', uri: 'https://i.ytimg.com/vi/cGFP4h1dD90/maxresdefault.jpg' },
//   { id: 6, text: 'Card #6', uri: 'https://i.ytimg.com/vi/cGFP4h1dD90/maxresdefault.jpg' },
//   { id: 7, text: 'Card #7', uri: 'https://i.ytimg.com/vi/cGFP4h1dD90/maxresdefault.jpg' },
//   { id: 8, text: 'Card #8', uri: 'https://i.ytimg.com/vi/cGFP4h1dD90/maxresdefault.jpg' },
// ];

// To see all the requests in the chrome Dev tools in the network tab.
XMLHttpRequest = GLOBAL.originalXMLHttpRequest ?
    GLOBAL.originalXMLHttpRequest :
    GLOBAL.XMLHttpRequest;

  // fetch logger
global._fetch = fetch;
global.fetch = function (uri, options, ...args) {
  return global._fetch(uri, options, ...args).then((response) => {
    console.log('Fetch', { request: { uri, options, ...args }, response });
    return response;
  });
};


class App extends Component {
      // componentDidMount(){
      //   var config = {
      //     apiKey: "AIzaSyBEtaIb7nh7dbpyycMrseToxVs4y8F6z_o",
      //     authDomain: "one-time-password-b9dab.firebaseapp.com",
      //     databaseURL: "https://one-time-password-b9dab.firebaseio.com",
      //     projectId: "one-time-password-b9dab",
      //     storageBucket: "one-time-password-b9dab.appspot.com",
      //     messagingSenderId: "338506015489"
      //   };
      //   firebase.initializeApp(config);
      // }
      static propTypes = {
        dispatch: PropTypes.func.isRequired,
        nav: PropTypes.object.isRequired,
      };
    
      componentDidMount() {
        initializeListeners('root', this.props.nav);
      }

      render() {
        const { dispatch, nav } = this.props;
        const navigation = navigationPropConstructor(dispatch, nav);
        //navigation = {navigation}
        return <AppNavigator />;
      }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
  // box: {
  //   height: boxHeight
  // }
});

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(App);
