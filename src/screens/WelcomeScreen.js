import React, { Component } from 'react';
import { View, Text, ActivityIndicator, StyleSheet, AsyncStorage } from 'react-native';
import _ from 'lodash';

import Slides from '../components/Slides';

const SLIDE_DATA = [
    { text: 'Welcome to JobApp', color: '#03A9F4' },
    { text: 'Use this to get a job', color: '#009688' },
    { text: 'Set your location, then swipe away', color: '#03A9F4' }
]

class WelcomeScreen extends Component {
    state = {
        token: null
    }

    async componentWillMount() {
        let token = await AsyncStorage.getItem('fb_token');

        if (token) {
            this.props.navigation.navigate('Map');
            this.setState({ token });
        } else {
            this.setState({ token: false })
        }
    }

    onSlidesComplete = () => {
        this.props.navigation.navigate("Auth");
    }

    render() {
        if(_.isNull(this.state.token)){
            return <View style={styles.loadingStyle}>
                <ActivityIndicator size="large" color="red"/>
            </View>
        }

        return (
            <Slides data={SLIDE_DATA} onComplete={this.onSlidesComplete}/>
        )
    }
}

const styles = StyleSheet.create({
    loadingStyle: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row'
    }
})

export default WelcomeScreen;