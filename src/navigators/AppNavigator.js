import React from 'react';

import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';

import AuthScreen from '../screens/AuthScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import MapScreen from '../screens/MapScreen';
import DeckScreen from '../screens/DeckScreen';
import SettingScreen from '../screens/SettingScreen';
import ReviewScreen from '../screens/ReviewScreen';

export const AppNavigator = createBottomTabNavigator({
  Welcome: { screen: WelcomeScreen },
  Auth: { screen: AuthScreen },
  Main: { 
    screen: createBottomTabNavigator({
      Map: { screen: MapScreen },
      Deck: { screen: DeckScreen },
      Review: {
        screen: createStackNavigator({
          Review: { screen: ReviewScreen },
          Settings: { screen: SettingScreen}
        })
      }
    })
  }
}, {
  navigationOptions: {
    tabBarVisible: false
  },
  lazy: true 
  //activate tab nao thi moi render ra tab do, neu ma = false thi se 
  //render het ra ngay lan dau tien
});