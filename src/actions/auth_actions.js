
import { AsyncStorage } from 'react-native'
import { LoginManager, AccessToken } from 'react-native-fbsdk'

import {
    FACEBOOK_LOGIN_SUCCESS, FACEBOOK_LOGIN_FAIL 
} from './types'

export const facebookLogin = () => async dispatch => {
    console.log("abc")
    console.log(AsyncStorage.getAllKeys())
    let token = await AsyncStorage.getItem('fb_token');
    if(token) {
        // dispatch an action saying FB login is done 
        dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token });
    } else {
        //start up fb login process
        doFacebookLogin(dispatch);
    }
}

const doFacebookLogin = async dispatch => {
    let result = await LoginManager.logInWithReadPermissions(['public_profile']);

    if(result.isCancelled) {
        return dispatch({ type: FACEBOOK_LOGIN_FAIL })
    } else {
        let val = await AccessToken.getCurrentAccessToken()

        await AsyncStorage.setItem('fb_token', val.accessToken);
        dispatch({ type: doFacebookLogin, payload: val.accessToken });
    }  
}