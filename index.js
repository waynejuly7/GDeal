import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import AppReducer from './src/reducers';
import App from './src/containers/App';
import { middleware } from './src/utils/redux';
import thunk from 'redux-thunk';

const store = createStore(
    AppReducer,
    applyMiddleware(middleware, thunk),
  );

class Root extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
        <Provider store={store}>
            <App />
        </Provider>
        );
    }
}

AppRegistry.registerComponent('GDeal', () => Root);
